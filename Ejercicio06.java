package recursividad;

public class Ejercicio06 {
	
	public static int fibonacci(int n) {
		if (n==0) {
			return 0;
			
		}else if (n==1) {
			return 1;
		}else {
			return n+fibonacci(n-1)+fibonacci(n-2);
		}
	}

	public static void main(String[] args) {
		//6. Calcular el valor de la posici�n fibonacci usando recursividad.
		System.out.println(fibonacci(10));

	}

}
