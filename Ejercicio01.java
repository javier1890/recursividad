package recursividad;

import java.util.Scanner;

public class Ejercicio01 {
	
	public static int sumar(int a){
		if (a==0) {
			return 0;
		}
		return a+sumar(a-1);
		
	}
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		//Sumar los n�meros n�meros naturales hasta N (se lo damos nosotros) de forma recursiva
		Scanner leer=new Scanner(System.in);
		int n;
		
		System.out.println("Ingresar la cantidad de numeros que desea ingresar");
		n=leer.nextInt();

		System.out.println(sumar(n));
	}

}
