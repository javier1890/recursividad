package recursividad;


public class Ejercicio05 {
	public static void recorrer(int i,int j,int m[][]) {
		
		System.out.println(m[i][j]+" ");
		
		if (i!=m.length-1 || j!=m[i].length-1) {
			if (j==m[i].length-1) {
				i++;
				j=0;
				System.out.println("\n");
			}else {
				j++;
			}
			recorrer(i, j, m);
		}
	}

	public static void main(String[] args) {
		// Recorrer una matriz de forma recursiva.
		
		int a[][]= {{5,1,2,3,9},{8,1,3,0,7}};
		recorrer(0,0,a);
		System.out.println("\n");
		System.out.println(a.length);
		

	}

}
