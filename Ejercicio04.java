package recursividad;

import java.util.Scanner;

public class Ejercicio04 {
	
	public static void buscar(int[] n,int i,int x) {
		if (i!=n.length) {
			if (i==x) {
				System.out.println(n[i]);
			}
			buscar(n,i+1,x);
		}
	}

	public static void main(String[] args) {
		//Buscar un elemento de un array de forma recursiva.
		
		int[] a= {2,5,4,6,7,10,9};
		
		Scanner leer=new Scanner(System.in);
		int n;
		
		System.out.println("Ingresar la posicion qe desea buscar:");
		n=leer.nextInt();
		
		buscar(a,0,n);
		
		

	}

}
