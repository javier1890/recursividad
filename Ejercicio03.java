package recursividad;

import java.util.Scanner;

import javax.swing.text.html.HTMLDocument.HTMLReader.FormAction;


public class Ejercicio03 {
	
	
	public static void recorrer(int[] n,int i) {
//		Forma 1
//		if (i==(n.length-1)) {
//			System.out.println(n[i]);
//		}else {
//			System.out.println(n[i]);
//			recorrer(n,i+1);
//		}	
		
		
//		Form 2
		if (i!=n.length) {
			System.out.println(n[i]);
			recorrer(n,i+1);
		}
	}

	public static void main(String[] args) {
		//Recorrer un array de forma recursiva.
		
		Scanner leer=new Scanner(System.in);
		
		
		int[] n={1,2,3,4,5,6,7,8};
		recorrer(n,0);
		
		
        
	}

}
